package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;

import org.w3c.dom.Text;

import static android.R.attr.keycode;

public class MyGdxGame extends ApplicationAdapter implements GestureListener, InputProcessor {
	SpriteBatch batch;
	TextureAtlas textAtlas;
	Animation animation;
	Sprite sprite;
	Sound idleengine;
	Sound runningengine;
	BitmapFont font;
	float posX;
	float posY;
	long soundID;
	long soundID2;
	float speed;
	float soundpitch;
	boolean accelerating;
	boolean isplaying;
	boolean isplayingIdle;
	enum flagenum{left, right, top, down};
	flagenum flagsus;


	private float elapsedTime = 0;

	@Override

	public void create() {
		batch = new SpriteBatch();
		textAtlas = new TextureAtlas(Gdx.files.internal("atlas/pack.atlas"));
		animation = new Animation(1/15f, textAtlas.getRegions());
		idleengine = Gdx.audio.newSound(Gdx.files.internal("Audio/engine-idle.wav"));
		runningengine = Gdx.audio.newSound(Gdx.files.internal("Audio/engine-running.wav"));
		accelerating = false;
		speed = 0;
		soundpitch = 0.5f;
		isplaying = false;
		isplayingIdle = false;
		font = new BitmapFont();

		InputMultiplexer im = new InputMultiplexer();
		GestureDetector gd = new GestureDetector(this);
		im.addProcessor(gd);
		im.addProcessor(this);
		Gdx.input.setInputProcessor(im);
	}

	@Override

	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		soundpitch =  0.5f + speed/201*1f;


		if(flagsus == flagenum.left){
			posX = posX - 50*Gdx.graphics.getDeltaTime();
		}else if(flagsus == flagenum.right)
		{
			posX = posX + 50*Gdx.graphics.getDeltaTime();
		}else if(flagsus == flagenum.down)
		{
			posY = posY - 50*Gdx.graphics.getDeltaTime();
		}
		else if(flagsus == flagenum.top)
		{
			posY = posY + 50*Gdx.graphics.getDeltaTime();
		}


		if(speed != 0)accelerating = true;
		else accelerating = false;

		if(accelerating) {
			isplayingIdle = false;
			runningengine.setLooping(soundID2, true);
			runningengine.setPitch(soundID2, soundpitch);
		}else{
			if(!isplayingIdle) {
				idleengine.play();
				soundID = idleengine.play();
				idleengine.setLooping(soundID, true);
				isplayingIdle = true;
			}
			runningengine.stop(soundID2);
			isplaying = false;
		}
		if(Gdx.input.isTouched())
		{
			if(!isplaying){
				runningengine.play();
				soundID2 = runningengine.play();
				isplaying = true;
			}
			if(isplayingIdle)idleengine.pause(soundID);
			if(speed < 200) {
				speed += 1;
			}
		}else {
			if (speed > 0) {
				speed -= 1;
			}
		}
		batch.begin();
		font.setColor(Color.WHITE);
		font.draw(batch, speed+" km/h", 25, 160);
		elapsedTime += Gdx.graphics.getDeltaTime();
		batch.draw((TextureRegion)animation.getKeyFrame(elapsedTime, true), posX, posY);
		batch.end();
	}

	@Override

	public void dispose() {
		batch.dispose();
		textAtlas.dispose();

	}

	@Override
	public boolean keyDown(int keycode) {

		if(keycode == Input.Keys.A){
			flagsus = flagenum.left;
		}else if(keycode == Input.Keys.D)
		{
			flagsus = flagenum.right;
		}else if(keycode == Input.Keys.S)
		{
			flagsus = flagenum.down;
		}
		else if(keycode == Input.Keys.W)
		{
			flagsus = flagenum.top;
		}
		return false;


	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public void pinchStop() {

	}
}